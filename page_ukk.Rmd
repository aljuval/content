---
title: "Usein kysytyt kysymykset"
output: 
  html_document: 
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---

# Sähköpostipolitiikka

>Voiko opettajalta kysyä sähköpostilla apua?

Ei. Käydään kurssiin liittyvä keskustelu [Slack](https://utur2016.slack.com/):ssä.

# Ennakkovaatimukset

>Pitääkö minun osata R:ää ennen kurssille tulemista

Suosittelen opettelmaan perusteita esimerkiksi [tryR](http://tryr.codeschool.com/):n tai [DataCamp](https://www.datacamp.com/courses/free-introduction-to-r):n avulla.

# Kurssin suorittaminen

>Pitääkö minun osallistua jokaiselle luennolle?

Kyllä

# Kotitehtävät

> Saako kotitehtäviin etsiä apua netistä?

Saa ja pitää


